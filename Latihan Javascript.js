//Soal 1


/* 

Direction:

Create a function that tests if a number is the exact upper bound of the factorial of n. If so, return an array of the exact factorial bound and n, or otherwise, the string "Not exact!".

*/

function faktorial(angka) {

    let mulaiFaktorial = 1;

    for (let i = 1; i <= angka; i++) {
      mulaiFaktorial *= i;

      if (angka === mulaiFaktorial) {
        return [angka, i];
      }
      if (angka < mulaiFaktorial) {
        return 'Bukan factorial!';
      }
    }
}


console.log(faktorial(6));
//  ➞ [6, 3]
console.log(faktorial(24));
// ➞ [24, 4]
console.log(faktorial(125));
// "Bukan faktorial!"
console.log(faktorial(720));
// ➞ [720, 6]
console.log(faktorial(1024));
// ➞ "Bukan faktorial!"
console.log(faktorial(40320))
// ➞ [40320, 8]





// Soal 2

/*

Direction:

Write a function that takes in a string and for each character, returns the distance to the nearest vowel in the string. If the character is a vowel itself, return 0.

Notes:
- All input strings will contain at least one vowel.
- Strings will be lowercased.
- Vowels are: a, e, i, o, u.

*/

function hurufVocal(vocal) {

    let jarakVocal = [];
    let setRegex = vocal.search(/[aeiou]/); //Set regex vocal
    
    for (let i = 0; i < vocal.length; i++) {
  
      let findVocal = vocal.slice(i).search(/[aeiou]/);
      let setValue = Math.abs(setRegex - i);
  
      if (findVocal !== -1 && setValue >= findVocal) {
          setRegex = findVocal + i;
      }
  
      jarakVocal.push(Math.abs(setRegex-i));
  
    }
  
    return jarakVocal;
  
  }
  
  console.log(hurufVocal("aaaaa"));
  //  ➞ [0, 0, 0, 0, 0]
  console.log(hurufVocal("babbb"));
  //  ➞ [1, 0, 1, 2, 3]
  console.log(hurufVocal("abcdabcd"));
  //  ➞ [0, 1, 2, 1, 0, 1, 2, 3]
  console.log(hurufVocal("shopper"));
  //  ➞ [2, 1, 0, 1, 1, 0, 1]


// Soal 3

/*

Create a function that takes a numeral (just digits without separators (e.g. 19093 instead of 19,093) and returns the standard way of reading a number, complete with punctuation.

*/

function rubahAngka() {

    let angkaNama = ['Zero' ,'One']

    let strAngka = angkanya.toString();
    let angkaStringNow = strAngka.split('');
  
    /// for(let i = 0; i <= angkaStringNow.length; i++) {
    //   // console.log(angkaStringNow[i]);
  
    //   if (angkaStringNow[i] === 0) {
    //     return 'Zero';
    //   } 
  
    //   if (angkaStringNow[i] === 11) {
    //     return 'Eleven';
    //   }
  
    // }

}

console.log(rubahAngka(0)); 
// ➞ "Zero."

console.log(rubahAngka(11)); 
// ➞ "Eleven."

console.log(rubahAngka(1043283)); 
// ➞ "One million, forty three thousand, two hundred and eighty three."

console.log(rubahAngka(90376000010012)); 
// ➞ "Ninety trillion, three hundred and seventy six billion, ten thousand and twelve."
// Soal 1

// Note: Please use ES6 to answer the question

/*

Diberikan function naikOjek(penumpang) yang merupakan sebuah multiple array Function akan me-return array of object.

Pembayaran akan di hitung jika Rp3000 jika melewati 1 KM. jika penumpang menggunakan ojek untuk bolak melanjutkan rute maka kalian juga harus menambahkan 'tujuan lanjutannya' dari si penumpang dan semua harga akan di kalkulasikan

contoh: 

Penumpang A bergerak sejauh 3km selama pergi, dan melanjutkan perjalanan sejauh 2km.

contoh input: 
[['A', 3km, 2km]] 

output: 
[{
    penumpang: 'A',  
    perjalanan: {
        jalanUtama: '3km', 
        jalan kedua: '2km'
    },
    totalBiaya: 15000
}]

*/


const naikOjek = (penumpang) => {

    // Your Code Here

}

console.log(naikOjek([
    ['Dimitri', '4km', '1km'],
    ['Icha', '5km', '2km']
]));



// Soal 2

/*
Implementasikan function graduates untuk mendapatkan daftar student yang lulus dengan aturan:

Direction:
- Siswa yang lulus 75
- Masukkan name dan score dari student ke class yang dia ikuti dan tambahkan indikator lulus.
- Untuk yang tidak lulus masukan name dan score lalu tambahkan indikator tidak lulus


[
    {
        <class>: [
            { name: <name>, score: <score>, status: <status> },
            ...
        ],
        <class>: [
            { name: <name>, score: <score>, status: <status> },
            ...
        ],
    },

    {
        <class>: [
            { name: <name>, score: <score>, status: <status> },
            ...
        ],
        <class>: [
            { name: <name>, score: <score>, status: <status> },
            ...
        ],
    }

]

*/


const graduates = (students) => {

    // code here

}

console.log(graduates([{
        name: 'Dimitri',
        score: 90,
        class: 'foxes'
    },
    {
        name: 'Alexei',
        score: 85,
        class: 'wolves'
    },
    {
        name: 'Sergei',
        score: 74,
        class: 'foxes'
    },
    {
        name: 'Anastasia',
        score: 78,
        class: 'wolves'
    }
]));


/*

Expect Output:
[
    {
        foxes: [
            { name: 'Dimitri', score: 90, status: 'lulus' }
        ],
        wolves: [
            { name: 'Alexei' , score: 85, status: 'lulus' },
            { name: 'Anastasia', score: 78, status: 'lulus' }
        ]
    },
    {
        foxes: [
            { name: 'Sergei', score: 90, status: 'tidak lulus' }
        ],
    }
]


*/


console.log(graduates([{
        name: 'Alexander',
        score: 100,
        class: 'foxes'
    },
    {
        name: 'Alisa',
        score: 76,
        class: 'wolves'
    },
    {
        name: 'Vladimir',
        score: 92,
        class: 'foxes'
    },
    {
        name: 'Albert',
        score: 71,
        class: 'wolves'
    },
    {
        name: 'Viktor',
        score: 80,
        class: 'tigers'
    }
]));


/*

Expect Output:
[
    {
        foxes: [
            { name: 'Alexander', score: 100, status: 'lulus' }
            { name: 'Vladimir', score: 92, status: 'lulus' }
        ],
        wolves: [
            { name: 'Alisa' , score: 76, status: 'lulus' },
        ],
        wolves: [
            { name: 'Alisa' , score: 76, status: 'lulus' },
        ]
    },
    {
        wolves: [
            { name: 'Albert', score: 71, status: 'tidak lulus' }
        ],
    }
]

*/


console.log(graduates([])); //{}
// Soal 2

/*

Direction:

- Buatlah sebuah function untuk array yang berisi kumpulan string
- Function akan me-return array multidimensi dimana array tersebut berisikan kategori/kelompok array yang dikumpulkan sesuai dengan abjad depannya (berurut a-z)


Contoh:
jika animals adalah ["ayam", "kucing", "bebek", "bangau", "zebra"]

maka akan menampilkan output: [ [ "ayam" ], [ "bebek", "bangau" ], [ "kucing ], [ "zebra" ] ]

*/


function groupAnimals(animals) {
    
    var abjadPertama = [];

    for (var i = 0; i < animals.length; i++) {
        abjadPertama.push(animals[i][0]);
    }

    abjadPertama.sort();
    // console.log(abjadPertama);

    var duplikat = [];
    for (var j = 0; j < abjadPertama.length; j++) {
        // console.log(abjadPertama[j]);
        if (abjadPertama[j] !== abjadPertama[j + 1]) {
            duplikat.push(abjadPertama[j]);
        }
    }

    var groups = [];

    for(var k = 0; k < duplikat.length; k++) {
        // console.log(duplikat[k]);
        var group = [];

        for (var group1 = 0; group1 < animals.length; group1++ ) {
            // console.log(animals[group1]);
            if (animals[group1][0] === duplikat[k]) {
                // console.log(animals[group1]);
                group.push(animals[group1]);
            }
        }
        // console.log(group);
        groups.push(group);
    }

    return groups;

}



// TEST CASES
console.log(groupAnimals(['cacing', 'ayam', 'kuda', 'anoa', 'kancil']));
// [ ['ayam', 'anoa'], ['cacing'], ['kuda', 'kancil'] ]
console.log(groupAnimals(['cacing', 'ayam', 'kuda', 'anoa', 'kancil', 'unta', 'cicak' ]));
// [ ['ayam', 'anoa'], ['cacing', 'cicak'], ['kuda', 'kancil'], ['unta'] ]
// Soal4

/*

Diberikan sebuah function hitungJumlahKata(kalimat) yang menerima parameter berupa string yang merupakan sebuah kalimat. Function akan mengembalikan jumlah kata dari kalimat tersebut. Contoh, "I have a dream" akan menghasilkan nilai 4, karena memiliki 4 kata.


*/

// function hitungJumlahKata(kalimat) {
    
//     return kalimat.split(' ').length;

//   }
  
//   // TEST CASES
//   console.log(hitungJumlahKata('I have a dream')); // 4
//   console.log(hitungJumlahKata('Never eat shredded wheat or cake')); // 6
//   console.log(hitungJumlahKata('A song to sing')); // 4
//   console.log(hitungJumlahKata('I')); // 1
//   console.log(hitungJumlahKata('I believe I can code')); // 5


const groupAnimals = (arr) => {

  let group = arr.sort().reduce((result, char) => {
    // console.log(result, 'ini result');
    // console.log(char, 'ini char');
    const letter = char[0];
    // console.log(letter, 'ini letter');
    // console.log(result[letter] || [], 'kaga tau');
    result[letter] = result[letter] || [];
    result[letter].push(char);
    return result;
  }, {});
  return group;
}

console.log(groupAnimals(['cacing', 'ayam', 'kuda', 'anoa', 'kancil']));

console.log(groupAnimals(['cacing', 'ayam', 'kuda', 'anoa', 'kancil', 'unta', 'cicak' ]));


var bali = [
	{ regency:'Jembrana', population:114863 },
	{ regency:'Tabanan', population:209308 },
	{ regency:'Badung', population:277536 },
	{ regency:'Gianyar', population:208443 },
	{ regency:'Klungkung', population:79233 },
	{ regency:'Bangli', population:106166 },
	{ regency:'Karangasem', population:193787 },
	{ regency:'Buleleng', population:294418 },
	{ regency:'Denpasar', population:403292 }
];

var sum = bali.reduce(function(val, element) {
  console.log(val, 'ini val');
  console.log(element, 'ini element');
	return val + element.population;
}, 0);

// console.log(sum) // 1887046



// Soal 1

/*

direction:

- Buat sebuah function dengan mempunyai parameter untuk menerima data array
- Argumen yang di terima merupakan kumpulan beberapa data array
- Implementasikanlah bentuk data menjadi sebuah kumpulan data seperti berikut ini :

Contoh Iput array:

//contoh input
var input = [
                ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
                ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
                ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
                ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
            ]

Dengan hasil:            

Nomor ID:  0001
Nama Lengkap:  Roman Alamsyah
TTL:  Bandar Lampung 21/05/1989
Hobi:  Membaca

Nomor ID:  0002
Nama Lengkap:  Dika Sembiring
TTL:  Medan 10/10/1992
Hobi:  Bermain Gitar

Nomor ID:  0003
Nama Lengkap:  Winona
TTL:  Ambon 25/12/1965
Hobi:  Memasak

Nomor ID:  0004
Nama Lengkap:  Bintang Senjaya
TTL:  Martapura 6/4/1970
Hobi:  Berkebun

*/

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]

function dataUser(dataUserArray) {

    var user = "";

    for (var i = 0; i <= dataUserArray.length - 1; i++) {
        console.log(dataUserArray[i]);
        user = user + 
        "Nomor ID: " + dataUserArray[i][0] + "\n" +
        "Nama Lengkap: " + dataUserArray[i][1] + "\n" +
        "TTL: " + dataUserArray[i][2] + "\n" +
        "Hobi: " + dataUserArray[i][3] + "\n\n"; 
    }

    return user;
}

console.log(dataUser(input));



